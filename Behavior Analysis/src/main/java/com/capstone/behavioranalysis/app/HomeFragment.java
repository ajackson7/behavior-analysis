package com.capstone.behavioranalysis.app;



import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.capstone.behavioranalysis.dtw.dtw.TimeWarpInfo;
import com.capstone.behavioranalysis.dtw.timeseries.TimeSeries;
import com.capstone.behavioranalysis.dtw.util.DistanceFunction;
import com.capstone.behavioranalysis.dtw.util.DistanceFunctionFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class HomeFragment extends android.app.Fragment implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    TextView status;
    private boolean timerOn;
    private FileWriter f;

    public HomeFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        status = (TextView) rootView.findViewById(R.id.status);

        final ToggleButton toggleBtn = (ToggleButton) rootView.findViewById(R.id.toggleBtn);
        toggleBtn.setOnCheckedChangeListener(


                new CompoundButton.OnCheckedChangeListener(){
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                        if(isChecked){
                            try {
                                f = new FileWriter("/sdcard/download/gesture.csv");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            new CountDownTimer(3000, 3000) {
                                public void onTick(long millisUntilFinished) {

                                }

                                public void onFinish() {
                                    try {
                                        f.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    onDTW();
                                    toggleBtn.setChecked(false);

                                }
                            }.start();

                        }
                    }
                });



        return rootView;
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        float x =  event.values[0];
        float y =  event.values[1];
        float z =  event.values[2];

        try {
            if (f != null) {
                f.append(event.values[0]+","+event.values[1]+","+event.values[2] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void onDTW(){
        final TimeSeries walk1 = new TimeSeries("/sdcard/download/walk1.csv", false, false, ',');
        final TimeSeries walk2 = new TimeSeries("/sdcard/download/walk2.csv", false, false, ',');
        final TimeSeries walk3 = new TimeSeries("/sdcard/download/walk3.csv", false, false, ',');
        final TimeSeries talk1 = new TimeSeries("/sdcard/download/talk1.csv", false, false, ',');
        final TimeSeries talk2 = new TimeSeries("/sdcard/download/talk2.csv", false, false, ',');
        final TimeSeries talk3 = new TimeSeries("/sdcard/download/talk3.csv", false, false, ',');
        final TimeSeries hand1 = new TimeSeries("/sdcard/download/hand1.csv", false, false, ',');
        final TimeSeries hand2 = new TimeSeries("/sdcard/download/hand2.csv", false, false, ',');
        final TimeSeries hand3 = new TimeSeries("/sdcard/download/hand3.csv", false, false, ',');
        final TimeSeries table1 = new TimeSeries("/sdcard/download/table1.csv", false, false, ',');
        final TimeSeries table2 = new TimeSeries("/sdcard/download/table2.csv", false, false, ',');
        final TimeSeries table3 = new TimeSeries("/sdcard/download/table3.csv", false, false, ',');
        final TimeSeries gesture = new TimeSeries("/sdcard/download/gesture.csv", false, false, ',');
        final DistanceFunction distFn = DistanceFunctionFactory.getDistFnByName("EuclideanDistance");
        ArrayList<TimeWarpInfo> info = new ArrayList<TimeWarpInfo>();

        info.add(0, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(walk1, gesture, 10, distFn));
        info.add(1, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(walk2, gesture, 10, distFn));
        info.add(2, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(walk3, gesture, 10, distFn));
        info.add(3, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(talk1, gesture, 10, distFn));
        info.add(4, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(talk2, gesture, 10, distFn));
        info.add(5, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(talk3, gesture, 10, distFn));
        info.add(6, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(hand1, gesture, 10, distFn));
        info.add(7, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(hand2, gesture, 10, distFn));
        info.add(8, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(hand3, gesture, 10, distFn));
        info.add(9, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(table1, gesture, 10, distFn));
        info.add(10, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(table2, gesture, 10, distFn));
        info.add(11, com.capstone.behavioranalysis.dtw.dtw.FastDTW.getWarpInfoBetween(table3, gesture, 10, distFn));

        ArrayList<TimeWarpInfo> compare = new ArrayList<TimeWarpInfo>();
        compare.addAll(0, info);
        double minDistance = info.get(0).getDistance();
        TimeWarpInfo minInfo = info.get(0);
        for(int i = 0; i < info.size() -1; i++){
            if (minDistance > info.get(i+1).getDistance()){
                minDistance = info.get(i+1).getDistance();
                minInfo = info.get(i+1);
            }
        }

        TimeWarpInfo result = minInfo;

        if (result.equals(compare.get(0)) || result.equals(compare.get(1)) || result.equals(compare.get(2))){
            status.setText("Status: walking");
        }

        if (result.equals(compare.get(3)) || result.equals(compare.get(4)) || result.equals(compare.get(5))){
            status.setText("Status: talking");
        }

        if (result.equals(compare.get(6)) || result.equals(compare.get(7)) || result.equals(compare.get(8))){
            status.setText("Status: in hand");
        }

        if (result.equals(compare.get(9)) || result.equals(compare.get(10)) || result.equals(compare.get(11))){
            status.setText("Status: on table");
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }



}
