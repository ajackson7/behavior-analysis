package com.capstone.behavioranalysis.app;



import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.capstone.behavioranalysis.sensors.*;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class SensorFragment extends android.app.Fragment {


    public SensorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sensor, container, false);

        final Button btnAcc = (Button) rootView.findViewById(R.id.btnAcc);
        final Button btnMagField = (Button) rootView.findViewById(R.id.btnMagField);
        final Button btnGyro = (Button) rootView.findViewById(R.id.btnGyro);
        final Button btnGrav = (Button) rootView.findViewById(R.id.btnGrav);
        final Button btnLinAcc = (Button) rootView.findViewById(R.id.btnLinAcc);
        final Button btnLight = (Button) rootView.findViewById(R.id.btnLight);
        final Button btnPressure = (Button) rootView.findViewById(R.id.btnPressure);
        final Button btnProx = (Button) rootView.findViewById(R.id.btnProx);
        final Button btnSound = (Button) rootView.findViewById(R.id.btnSound);

        View.OnClickListener handler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btnAcc){
                    Intent intentAcc = new Intent(getActivity(), AccActivity.class);
                    getActivity().startActivity(intentAcc);
                }

                    if (v == btnMagField) {
                        Intent intentMag = new Intent(getActivity(), MagActivity.class);
                        getActivity().startActivity(intentMag);
                    }

                    if (v == btnGyro) {
                        Intent intentGyro = new Intent(getActivity(), GyroActivity.class);
                        getActivity().startActivity(intentGyro);
                    }

                    if (v == btnGrav) {
                        Intent intentGrav = new Intent(getActivity(), GravActivity.class);
                        getActivity().startActivity(intentGrav);
                    }

                    if (v == btnLinAcc) {
                        Intent intentLinAcc = new Intent(getActivity(), LinAccActivity.class);
                        getActivity().startActivity(intentLinAcc);
                    }

                    if (v == btnLight) {
                        Intent intentLight = new Intent(getActivity(), LightActivity.class);
                        getActivity().startActivity(intentLight);
                    }

                    if (v == btnPressure) {
                        Intent intentPressure = new Intent(getActivity(), PressureActivity.class);
                        getActivity().startActivity(intentPressure);
                    }

                    if (v == btnProx) {
                        Intent intentProx = new Intent(getActivity(), ProxActivity.class);
                        getActivity().startActivity(intentProx);
                    }

                    if (v == btnSound) {
                        Intent intentProx = new Intent(getActivity(), SoundActivity.class);
                        getActivity().startActivity(intentProx);
                    }
            }
        };

        btnAcc.setOnClickListener(handler);
        btnMagField.setOnClickListener(handler);
        btnGyro.setOnClickListener(handler);
        btnGrav.setOnClickListener(handler);
        btnLinAcc.setOnClickListener(handler);
        btnLight.setOnClickListener(handler);
        btnPressure.setOnClickListener(handler);
        btnProx.setOnClickListener(handler);
        btnSound.setOnClickListener(handler);

        return rootView;
    }


}
