package com.capstone.behavioranalysis.app;



import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.FileWriter;
import java.io.IOException;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class TrainFragment extends android.app.Fragment implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    TextView txtWalk, txtTalk, txtHand, txtTable;
    FileWriter f;
    int cntWalk = 1;
    int cntTalk = 1;
    int cntHand = 1;
    int cntTable = 1;

    public TrainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_train, container, false);

        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        txtWalk=(TextView) rootView.findViewById(R.id.txtWalk);
        txtTalk=(TextView) rootView.findViewById(R.id.txtTalk);
        txtHand=(TextView) rootView.findViewById(R.id.txtHand);
        txtTable=(TextView) rootView.findViewById(R.id.txtTable);


        final ToggleButton btnWalk = (ToggleButton)rootView.findViewById(R.id.btnWalk);
        btnWalk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    try{
                        f = new FileWriter("/sdcard/download/walk" + cntWalk + ".csv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (cntWalk == 4){
                        cntWalk = 1;
                    }
                    txtWalk.setText("Saving to walk" + cntWalk + ".csv");
                    cntWalk++;
                } else {
                    try{
                        f.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtWalk.setText("");
                }
            }
        });

        final ToggleButton btnTalk = (ToggleButton)rootView.findViewById(R.id.btnTalk);
        btnTalk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    try {
                        f = new FileWriter("/sdcard/download/talk" + cntTalk + ".csv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (cntTalk == 4) {
                        cntTalk = 1;
                    }
                    txtTalk.setText("Saving to talk" + cntTalk + ".csv");
                    cntTalk++;
                } else {
                    try {
                        f.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtTalk.setText("");
                }
            }
        });

        final ToggleButton btnHand = (ToggleButton)rootView.findViewById(R.id.btnHand);
        btnHand.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    try{
                        f = new FileWriter("/sdcard/download/hand" + cntHand + ".csv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (cntHand == 4){
                        cntHand = 1;
                    }
                    txtHand.setText("Saving to hand" + cntHand + ".csv");
                    cntHand++;
                } else {
                    try{
                        f.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtHand.setText("");
                }
            }
        });

        final ToggleButton btnTable = (ToggleButton)rootView.findViewById(R.id.btnTable);
        btnTable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    try{
                        f = new FileWriter("/sdcard/download/table" + cntTable + ".csv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (cntTable == 4){
                        cntTable = 1;
                    }
                    txtTable.setText("Saving to table" + cntTable + ".csv");
                    cntTable++;
                } else {
                    try{
                        f.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtTable.setText("");
                }
            }
        });

        return rootView;
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        float x =  event.values[0];
        float y =  event.values[1];
        float z =  event.values[2];

        try {
            if (f != null) {
                f.append(event.values[0]+","+event.values[1]+","+event.values[2] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

}
