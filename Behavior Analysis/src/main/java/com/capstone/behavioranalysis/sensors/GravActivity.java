package com.capstone.behavioranalysis.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.capstone.behavioranalysis.app.R;

import java.io.FileWriter;
import java.io.IOException;


public class GravActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mGravity;
    TextView xAxis,yAxis,zAxis;
    FileWriter f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gravity);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        xAxis=(TextView)findViewById(R.id.xAxis);
        yAxis=(TextView)findViewById(R.id.yAxis);
        zAxis=(TextView)findViewById(R.id.zAxis);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        // Many sensors return 3 values, one for each axis.
        float x =  event.values[0];
        float y =  event.values[1];
        float z =  event.values[2];

        //display values using TextView
        xAxis.setText("\t\t"+x);
        yAxis.setText("\t\t" +y);
        zAxis.setText("\t\t" +z);

        try {
            if (f != null) {
                f.append(event.values[0]+" "+event.values[1]+" "+event.values[2] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSave( View view){
        try{
            f = new FileWriter("/sdcard/download/gravity.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStop(View view){
        try{
            if (f!= null)
                f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mGravity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}