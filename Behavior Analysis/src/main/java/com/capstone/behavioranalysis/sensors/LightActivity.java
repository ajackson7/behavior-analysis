package com.capstone.behavioranalysis.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.capstone.behavioranalysis.app.R;

import java.io.FileWriter;
import java.io.IOException;


public class LightActivity extends Activity implements SensorEventListener{
    private SensorManager mSensorManager;
    private Sensor mLight;
    TextView lightValue;
    FileWriter f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.light);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        lightValue=(TextView)findViewById(R.id.lightData);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        // Many sensors return 3 values, one for each axis.
        float value =  event.values[0];

        //display values using TextView
        lightValue.setText("\t\t" + value);

        try {
            if (f != null) {
                f.append(event.values[0] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSave( View view){
        try{
            f = new FileWriter("/sdcard/download/light.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStop(View view){
        try{
            if (f!= null)
                f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}