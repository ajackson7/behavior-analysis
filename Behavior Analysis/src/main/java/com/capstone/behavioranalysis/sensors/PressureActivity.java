package com.capstone.behavioranalysis.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.capstone.behavioranalysis.app.R;

import java.io.FileWriter;
import java.io.IOException;


public class PressureActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mPressure;
    TextView pressureValue;
    FileWriter f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pressure);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        pressureValue=(TextView)findViewById(R.id.pressureData);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        // Many sensors return 3 values, one for each axis.
        float value =  event.values[0];

        //display values using TextView
        pressureValue.setText("\t\t" + value);

        try {
            if (f != null) {
                f.append(event.values[0] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSave( View view){
        try{
            f = new FileWriter("/sdcard/download/pressure.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStop(View view){
        try{
            if (f!= null)
                f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}