package com.capstone.behavioranalysis.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.capstone.behavioranalysis.app.R;

import java.io.FileWriter;
import java.io.IOException;


public class ProxActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mProximity;
    TextView proximityValue;
    FileWriter f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proximity);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        proximityValue=(TextView)findViewById(R.id.proximityData);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        float value =  event.values[0];

        if (value == 5.0) proximityValue.setText("Far");
        if (value == 0.0) proximityValue.setText("Near");
       // proximityValue.setText("\t\t" + value);

        try {
            if (f != null) {
                f.append(event.values[0] + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSave( View view){
        try{
            f = new FileWriter("/sdcard/download/proximity.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStop(View view){
        try{
            if (f!= null)
                f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}