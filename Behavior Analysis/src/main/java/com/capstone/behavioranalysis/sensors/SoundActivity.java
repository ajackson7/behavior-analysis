package com.capstone.behavioranalysis.sensors;

import android.app.Activity;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import com.capstone.behavioranalysis.app.R;

import java.io.FileWriter;
import java.io.IOException;


public class SoundActivity extends Activity {
    private MediaRecorder mRecorder = null;
    TextView soundData;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sound);
        soundData = (TextView) findViewById(R.id.soundData);

    }


    public void onSave( View view){
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile("/dev/null");
            try {
                mRecorder.prepare();
                mRecorder.start();
            } catch (IOException e){
                e.printStackTrace();
            }

        }
        getAmplitude();
    }

    public void onStop(View view){
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    public void getAmplitude() {
        new CountDownTimer(30000, 500) {
            public void onTick(long millisUntilFinished) {
                if (mRecorder != null)
                    soundData.setText(String.valueOf(mRecorder.getMaxAmplitude()));
            }

            public void onFinish() {
                soundData.setText("done");

            }
        }.start();


    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}